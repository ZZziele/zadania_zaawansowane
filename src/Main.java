package src;

import src.Kształty.Circle;
import src.Kształty.Line;
import src.Kształty.Rectangle;
import src.Kształty.Shape;
import src.Kształty.Square;
import src.Ludzie.Person;
import src.Ludzie.Staff;
import src.Ludzie.Student;
import src.Punkty.Movable;
import src.Punkty.MovableCircle;
import src.Punkty.MovePoint;
import src.Punkty.Point2D;
import src.Punkty.Point3D;

public class Main {
    public static void main(String[] args) {

        Circle circleGeometricObject = new Circle( "Blue", true,14);
        System.out.println(circleGeometricObject.getArea());
        System.out.println(circleGeometricObject.getPerimeter());
        System.out.println(circleGeometricObject);

        circleGeometricObject.resize(50);


        System.out.println(circleGeometricObject.getArea());
        System.out.println(circleGeometricObject.getPerimeter());


        System.out.println(circleGeometricObject);
        System.out.println(circleGeometricObject.toString());

    }

}
