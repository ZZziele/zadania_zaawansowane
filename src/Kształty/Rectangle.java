package src.Kształty;

public class Rectangle extends Shape{
   protected double width;

   protected double length;

    public Rectangle() {
        super();
        width = 1.0;
        length = 1.0;
    }

    public Rectangle(String colour, boolean isFilled, double width, double length) {
        super(colour, isFilled);
        this.width = width;
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea(){
        return width*length;
    }

    public double getPerimeter(){
        return 2*width + 2*length;
    }

    @Override
    public String toString() {
        return String.format("Rectangle with width=%f and length=%f which is a subclass off %s",width,length,super.toString());
    }
}
