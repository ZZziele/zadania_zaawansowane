package src.Kształty;

public abstract class Shape  {
    protected String colour;

    protected boolean isFilled;

    public Shape(){
        colour = "unknown";
        isFilled = false;
    }

    public Shape(String colour, boolean isFilled) {
        this.colour = colour;
        this.isFilled = isFilled;
    }

    public String getColour() {
        return colour;
    }

    public boolean isFilled() {
        return isFilled;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public void setFilled(boolean filled) {
        isFilled = filled;
    }

    @Override
    public String toString() {
        if(isFilled){
            return "Shape with color of " + colour + " and  filled";
        }else{
            return "Shape with color of " + colour + " and  Not Filled";
        }
    }

    public abstract double getArea();

    public abstract double getPerimeter();


}
