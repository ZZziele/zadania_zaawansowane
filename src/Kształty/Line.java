package src.Kształty;

import src.Punkty.Point2D;

import java.nio.channels.Pipe;

public class Line {
    protected Point2D point1;

    protected Point2D point2;

    public Line(Point2D ponint1, Point2D point2) {
        this.point1 = ponint1;
        this.point2 = point2;
    }

    public  Line(float x1,float y1 , float x2 , float y2) {
        point1 = new Point2D(x1,y1);
        point2 = new Point2D(x2,y2);
    }

    public Point2D getPoint1() {
        return point1;
    }

    public Point2D getPoint2() {
        return point2;
    }

    public void setPoint1(Point2D point1) {
        this.point1 = point1;
    }

    public void setPoint2(Point2D point2) {
        this.point2 = point2;
    }

    public float getLength(){
        return (float) Math.sqrt( Math.pow(point1.getX() - point2.getX(), 2 )  +  Math.pow( point1.getY() - point2.getX(), 2 ) );
    }

    public  Point2D getMiddlePoint (){
        return  new Point2D((point1.getX() - point2.getX())/2 , (point1.getY()-point2.getY())/2);
    }
}
