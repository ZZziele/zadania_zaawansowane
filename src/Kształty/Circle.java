package src.Kształty;

import src.Punkty.GeometricObject;
import src.Punkty.ResizableCircle;

public class Circle extends Shape implements GeometricObject , ResizableCircle {
    private double radius;


    public Circle() {
        super();
        radius = 1.0;
    }

    public Circle(String colour, boolean isFilled, double radius) {
        super(colour, isFilled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return  Math.pow(radius,2)*Math.PI;
    }

    public double getPerimeter(){
        return 2*Math.PI*radius;
    }

    @Override
    public void resize(int percent) {
        radius = radius * percent/100.0;
    }

    @Override
    public String toString() {
        return "Circle wth radious = " + radius + " which is a subclass of " + super.toString();
    }
}
