package src.Punkty;

public interface GeometricObject {

    double getPerimeter();

    double getArea();
}
