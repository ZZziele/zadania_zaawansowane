package src.Punkty;

public  class MovableCircle extends MovePoint implements  Movable  {

    float radiant;
    MovePoint movePoint;


    public MovableCircle(int x, int y, int xSpeed, int ySpeed, float radiant) {
        super(x, y, xSpeed, ySpeed);
        this.radiant = radiant;

    }

    @Override
    public void moveUp() {
        super.moveUp();
    }

    @Override
    public void moveDown() {
        super.moveDown();
    }

    @Override
    public void moveLeft() {
        super.moveLeft();
    }

    @Override
    public void moveRight() {
        super.moveRight();
    }

    @Override
    public String toString() {
        return "MovableCircle{" +
                "radiant=" + radiant +
                ", movePoint=" + super.toString() +
                '}';
    }
}
