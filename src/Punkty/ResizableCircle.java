package src.Punkty;

public interface ResizableCircle {
    public void resize(int percent);
}
