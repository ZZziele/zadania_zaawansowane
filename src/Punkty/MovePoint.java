package src.Punkty;

public  class MovePoint implements Movable {
    private int x, y;
    private int xSpeed, ySpeed;

    public MovePoint(int x, int y, int xSpeed, int ySpeed) {
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }


    @Override
    public void moveUp() {
            y = y + ySpeed;
    }

    @Override
    public void moveDown() {
            y = y - ySpeed;
    }

    @Override
    public void moveLeft() {
            x = x - xSpeed;
    }

    @Override
    public void moveRight() {
        x = x + xSpeed;
    }

    @Override
    public String toString() {
        return "MovePoint{" +
                "x=" + x +
                ", y=" + y +
                ", xSpeed=" + xSpeed +
                ", ySpeed=" + ySpeed +
                '}';
    }
}
