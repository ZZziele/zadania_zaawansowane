package src.Punkty;

public interface Resizable {
    public void resize(int percent);
}
