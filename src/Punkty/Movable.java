package src.Punkty;

public interface Movable {
    void moveUp();

    void moveDown();

    void moveLeft();

    void moveRight();
}
