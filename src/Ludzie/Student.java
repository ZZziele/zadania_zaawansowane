package src.Ludzie;

public class Student extends Person {
    private String typeOfStudy;

    private int yearOfStudy;

    private float costOfStudy;

    public Student(String name, String address, String typeOfStudy, int yearOfStudy, float costOfStudy) {
        super(name, address);
        this.typeOfStudy = typeOfStudy;
        this.yearOfStudy = yearOfStudy;
        this.costOfStudy = costOfStudy;
    }

    public String getTypeOfStudy() {
        return typeOfStudy;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public float getCostOfStudy() {
        return costOfStudy;
    }

    public void setTypeOfStudy(String typeOfStudy) {
        this.typeOfStudy = typeOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }

    public void setCostOfStudy(float costOfStudy) {
        this.costOfStudy = costOfStudy;
    }

    @Override
    public String toString() {
        return "Student{" +
                "typeOfStudy='" + typeOfStudy + '\'' +
                ", yearOfStudy=" + yearOfStudy +
                ", costOfStudy=" + costOfStudy +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
